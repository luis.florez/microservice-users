package com.example.microserviceusers.domain.validation;

import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.infrastructure.exceptions.*;
import org.junit.Assert;
import org.junit.Test;
import java.time.LocalDate;

public class UserValidationTest {

    @Test
    public void validateUserPresent_when_is_present() {
        UserValidation.validateUserPresent(new User());
    }

    @Test(expected = UserEmptyException.class)
    public void validateUserPresent_when_is_empty() {
        UserValidation.validateUserPresent(null);
    }

    @Test
    public void validateIdRoleOwner_when_is_equal() {
        Assert.assertTrue(UserValidation.validateIdRoleOwner(1L));
    }

    @Test
    public void validateIdRoleOwner_when_is_different() {
        Assert.assertFalse(UserValidation.validateIdRoleOwner(2L));
    }

    @Test
    public void validateRequiredField_when_data_is_not_null() {
        UserValidation.validateRequiredField("", "");
    }

    @Test(expected = FieldRequiredException.class)
    public void validateRequiredField_when_data_is_null() {
        UserValidation.validateRequiredField(null, "");
    }

    @Test
    public void validatePhone_when_phone_with_further_in_init() {
        UserValidation.validatePhone("+5454844");
    }

    @Test
    public void validatePhone_when_phone_only_has_numbers() {
        UserValidation.validatePhone("5454844");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_not_only_has_numbers() {
        UserValidation.validatePhone("545484wfaraqq*4");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_with_further_not_in_init() {
        UserValidation.validatePhone("54548+44");
    }

    @Test(expected = InvalidPhoneException.class)
    public void validatePhone_when_phone_only_has_numbers_and_spaces() {
        UserValidation.validatePhone("545 484 48888");
    }

    @Test(expected = PhoneLengthException.class)
    public void validatePhoneLength_when_length_is_less_than_7() {
        UserValidation.validatePhoneLength("123");
    }

    @Test
    public void validatePhoneLength_when_length_is_equal_to_7() {
        UserValidation.validatePhoneLength("1234567");
    }

    @Test
    public void validatePhoneLength_when_length_it_is_between_7_and_13() {
        UserValidation.validatePhoneLength("12345678910");
    }

    @Test
    public void validatePhoneLength_when_length_is_equal_to_13() {
        UserValidation.validatePhoneLength("1234567891234");
    }

    @Test(expected = PhoneLengthException.class)
    public void validatePhoneLength_when_length_is_greater_than_13() {
        UserValidation.validatePhoneLength("123456789123456789");
    }

    @Test
    public void validateNumberDocument_when_only_has_numbers() {
        UserValidation.validateNumberDocument("651561115451");
    }

    @Test(expected = InvalidNumberDocumentException.class)
    public void validateNumberDocument_when_not_only_has_numbers() {
        UserValidation.validateNumberDocument("651561115a451");
    }

    @Test
    public void validateEmailValid_when_email_is_complete() {
        UserValidation.validateEmailValid("aaa@gmail.com");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_is_empty() {
        UserValidation.validateEmailValid("");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_not_have_name() {
        UserValidation.validateEmailValid("@gmail.com");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_not_have_at_sign() {
        UserValidation.validateEmailValid("aaagmail.com");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_not_have_domain() {
        UserValidation.validateEmailValid("aaa@");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_not_have_organization() {
        UserValidation.validateEmailValid("aaa@.com");
    }

    @Test(expected = InvalidEmailException.class)
    public void validateEmailValid_when_not_have_type() {
        UserValidation.validateEmailValid("aaa@gmail");
    }

    @Test(expected = UserNotAdultException.class)
    public void validateAdult_when_user_have_less_than_18_years() {
        UserValidation.validateAdult(LocalDate.of(2002, 8, 1), LocalDate.of(2020, 7, 30));
    }

    @Test
    public void validateAdult_when_user_have_18_years() {
        UserValidation.validateAdult(LocalDate.of(2002, 8, 1), LocalDate.of(2020, 8, 30));
    }

    @Test
    public void validateAdult_when_user_have_more_of_18_years() {
        UserValidation.validateAdult(LocalDate.of(1991, 8, 1), LocalDate.of(2020, 7, 30));
    }
}