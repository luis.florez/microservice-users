package com.example.microserviceusers.infrastructure.configuration;

import com.example.microserviceusers.domain.api.IUserServicePort;
import com.example.microserviceusers.domain.spi.IUserPersistencePort;
import com.example.microserviceusers.domain.usecase.UserUseCase;
import com.example.microserviceusers.infrastructure.out.jpa.adapter.UserJpaAdapter;
import com.example.microserviceusers.infrastructure.out.jpa.mapper.UserEntityMapper;
import com.example.microserviceusers.infrastructure.out.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final IUserRepository userRepository;
    private final UserEntityMapper userEntityMapper;

    @Bean
    public IUserPersistencePort userPersistencePort() {
        return new UserJpaAdapter(userRepository, userEntityMapper);
    }

    @Bean
    public IUserServicePort userServicePort() {
        return new UserUseCase(userPersistencePort());
    }
}