package com.example.microserviceusers.infrastructure.exceptions;

public class UserNotAdultException extends RuntimeException {

    public UserNotAdultException() {
        super();
    }
}
