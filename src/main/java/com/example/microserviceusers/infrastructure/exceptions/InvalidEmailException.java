package com.example.microserviceusers.infrastructure.exceptions;

public class InvalidEmailException extends RuntimeException {

    public InvalidEmailException() {
        super();
    }
}
