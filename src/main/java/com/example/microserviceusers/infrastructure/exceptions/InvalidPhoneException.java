package com.example.microserviceusers.infrastructure.exceptions;

public class InvalidPhoneException extends RuntimeException {

    public InvalidPhoneException() {
        super();
    }
}
