package com.example.microserviceusers.infrastructure.exceptions;

public class UserEmptyException extends RuntimeException {

    public UserEmptyException() {
        super();
    }
}
