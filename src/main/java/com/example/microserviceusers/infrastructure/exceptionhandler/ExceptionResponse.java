package com.example.microserviceusers.infrastructure.exceptionhandler;

public enum ExceptionResponse {

    PHONE_LENGTH_INCORRECT("The phone number must be between 7 and 13 characters long"),
    INVALID_EMAIL("The email entered is not valid"),
    INVALID_PHONE("The phone entered is not valid"),
    USER_NOT_FOUND("The user was not found"),
    INVALID_NUMBER_DOCUMENT("The number document entered must be numeric only"),
    FIELD_REQUIRED("The field is required: "),
    USER_NOT_ADULT("The user to register is not adult");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}