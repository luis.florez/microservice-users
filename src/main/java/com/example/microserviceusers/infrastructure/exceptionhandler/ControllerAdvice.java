package com.example.microserviceusers.infrastructure.exceptionhandler;

import com.example.microserviceusers.infrastructure.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.Collections;
import java.util.Map;

@RestControllerAdvice
public class ControllerAdvice {

    private static final String MESSAGE = "message";

    @ExceptionHandler(FieldRequiredException.class)
    public ResponseEntity<Map<String, String>> handlerFieldRequiredException(FieldRequiredException fieldRequiredException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.FIELD_REQUIRED.getMessage() + fieldRequiredException.getTypeData()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidEmailException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidEmailException(InvalidEmailException invalidEmailException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_EMAIL.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidNumberDocumentException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidNumberDocumentException(InvalidNumberDocumentException invalidNumberDocumentException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_NUMBER_DOCUMENT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidPhoneException.class)
    public ResponseEntity<Map<String, String>> handlerInvalidPhoneException(InvalidPhoneException invalidPhoneException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_PHONE.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PhoneLengthException.class)
    public ResponseEntity<Map<String, String>> handlerPhoneLengthException(PhoneLengthException phoneLengthException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.PHONE_LENGTH_INCORRECT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotAdultException.class)
    public ResponseEntity<Map<String, String>> handlerUserNotAdultException(UserNotAdultException userNotAdultException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_ADULT.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserEmptyException.class)
    public ResponseEntity<Map<String, String>> handlerUserEmptyException(UserEmptyException userEmptyException) {
        return new ResponseEntity<>(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()), HttpStatus.NOT_FOUND);
    }
}
