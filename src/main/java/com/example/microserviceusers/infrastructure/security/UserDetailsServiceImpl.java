package com.example.microserviceusers.infrastructure.security;

import com.example.microserviceusers.application.dto.UserResponse;
import com.example.microserviceusers.application.handler.IUserHandler;
import com.example.microserviceusers.infrastructure.constant.SecurityConstant;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final IUserHandler userHandler;

    public UserDetailsServiceImpl(IUserHandler userHandler) {
        this.userHandler = userHandler;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserResponse userResponse = userHandler.getUserByEmail(email);
        if (userResponse != null) return new UserDetailsImpl(userResponse);
        else throw new UsernameNotFoundException(SecurityConstant.USER_NOT_FOUND);
    }
}
