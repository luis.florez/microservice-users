package com.example.microserviceusers.infrastructure.input.rest;

import com.example.microserviceusers.application.dto.UserRequest;
import com.example.microserviceusers.application.dto.UserResponse;
import com.example.microserviceusers.application.handler.IUserHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {

    private final IUserHandler userHandler;

    @PostMapping("/create-owner")
    public ResponseEntity<Void> saveUserOwner(@RequestBody UserRequest userRequest) {
        userHandler.saveUserOwner(userRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/create-employee")
    public ResponseEntity<Long> saveUserEmployee(@RequestBody UserRequest userRequest) {
        return new ResponseEntity<>(userHandler.saveUserEmployee(userRequest), HttpStatus.CREATED);
    }

    @PostMapping("/create-customer")
    public ResponseEntity<Void> saveUserCustomer(@RequestBody UserRequest userRequest) {
        userHandler.saveUserCustomer(userRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/role/{idUser}")
    public ResponseEntity<Boolean> existUserWithRoleOwner(@PathVariable Long idUser) {
        return new ResponseEntity<>(userHandler.existUserWithRoleOwner(idUser), HttpStatus.OK);
    }

    @GetMapping("/exists/{id}")
    public ResponseEntity<Boolean> existUserWithId(@PathVariable Long id) {
        return new ResponseEntity<>(userHandler.existUserWithId(id), HttpStatus.OK);
    }

    @GetMapping("/username")
    public ResponseEntity<UserResponse> getUserByEmail(@RequestParam String email) {
        return new ResponseEntity<>(userHandler.getUserByEmail(email), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<UserResponse> getUserById(@RequestParam Long id) {
        return new ResponseEntity<>(userHandler.getUserById(id), HttpStatus.OK);
    }
}
