package com.example.microserviceusers.infrastructure.out.jpa.mapper;

import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.infrastructure.out.jpa.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserEntityMapper {

    UserEntity toEntity(User user);

    User toModel(UserEntity userEntity);
}
