package com.example.microserviceusers.infrastructure.out.jpa.repository;

import com.example.microserviceusers.infrastructure.out.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String email);
}
