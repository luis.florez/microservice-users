package com.example.microserviceusers.infrastructure.out.jpa.adapter;

import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.domain.spi.IUserPersistencePort;
import com.example.microserviceusers.infrastructure.out.jpa.mapper.UserEntityMapper;
import com.example.microserviceusers.infrastructure.out.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserJpaAdapter implements IUserPersistencePort {

    private final IUserRepository userRepository;
    private final UserEntityMapper userEntityMapper;

    @Override
    public Long saveUser(User user) {
        return userRepository.save(userEntityMapper.toEntity(user)).getId();
    }

    @Override
    public User getUserById(Long idUser) {
        return userEntityMapper.toModel(userRepository.findById(idUser).orElse(null));
    }

    @Override
    public User getUserByEmail(String email) {
        return userEntityMapper.toModel(userRepository.findByEmail(email));
    }
}
