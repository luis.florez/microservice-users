package com.example.microserviceusers.application.handler;

import com.example.microserviceusers.application.dto.UserRequest;
import com.example.microserviceusers.application.dto.UserResponse;

public interface IUserHandler {

    void saveUserOwner(UserRequest userRequest);

    Long saveUserEmployee(UserRequest userRequest);

    void saveUserCustomer(UserRequest userRequest);

    boolean existUserWithRoleOwner(Long idUser);

    boolean existUserWithId(Long id);

    UserResponse getUserByEmail(String email);

    UserResponse getUserById(Long id);
}
