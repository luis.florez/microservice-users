package com.example.microserviceusers.application.handler.impl;

import com.example.microserviceusers.application.dto.UserRequest;
import com.example.microserviceusers.application.dto.UserResponse;
import com.example.microserviceusers.application.handler.IUserHandler;
import com.example.microserviceusers.application.mapper.UserRequestMapper;
import com.example.microserviceusers.application.mapper.UserResponseMapper;
import com.example.microserviceusers.domain.api.IUserServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler {

    private final IUserServicePort userServicePort;
    private final UserRequestMapper userRequestMapper;
    private final UserResponseMapper userResponseMapper;

    @Override
    public void saveUserOwner(UserRequest userRequest) {
        userServicePort.saveUserOwner(userRequestMapper.toUser(userRequest));
    }

    @Override
    public Long saveUserEmployee(UserRequest userRequest) {
        return userServicePort.saveUserEmployee(userRequestMapper.toUser(userRequest));
    }

    @Override
    public void saveUserCustomer(UserRequest userRequest) {
        userServicePort.saveUserCustomer(userRequestMapper.toUser(userRequest));
    }

    @Override
    public boolean existUserWithRoleOwner(Long idUser) {
        return userServicePort.existUserWithRoleOwner(idUser);
    }

    @Override
    public boolean existUserWithId(Long id) {
        return userServicePort.existUserWithId(id);
    }

    @Override
    public UserResponse getUserByEmail(String email) {
        return userResponseMapper.toResponse(userServicePort.getUserByEmail(email));
    }

    @Override
    public UserResponse getUserById(Long id) {
        return userResponseMapper.toResponse(userServicePort.getUserById(id));
    }
}
