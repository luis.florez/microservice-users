package com.example.microserviceusers.application.mapper;

import com.example.microserviceusers.application.dto.UserRequest;
import com.example.microserviceusers.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "idRole", ignore = true)
    User toUser(UserRequest userRequest);
}

