package com.example.microserviceusers.application.mapper;

import com.example.microserviceusers.application.dto.UserResponse;
import com.example.microserviceusers.domain.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserResponseMapper {

    UserResponse toResponse(User user);
}

