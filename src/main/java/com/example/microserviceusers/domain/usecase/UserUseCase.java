package com.example.microserviceusers.domain.usecase;

import com.example.microserviceusers.domain.api.IUserServicePort;
import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.domain.model.constant.UserUseCaseConstant;
import com.example.microserviceusers.domain.spi.IUserPersistencePort;
import com.example.microserviceusers.domain.validation.UserValidation;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.time.LocalDate;

public class UserUseCase implements IUserServicePort {

    private final IUserPersistencePort userPersistencePort;


    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    public Long saveUser(User user, Long idRole) {
        UserValidation.validateRequiredField(user.getName(), UserUseCaseConstant.DATA_NAME);
        UserValidation.validateRequiredField(user.getLastName(), UserUseCaseConstant.DATA_LAST_NAME);
        UserValidation.validateRequiredField(user.getNumberDocument(), UserUseCaseConstant.DATA_DOCUMENT_NUMBER);
        UserValidation.validateRequiredField(user.getPhone(), UserUseCaseConstant.DATA_PHONE);
        UserValidation.validateRequiredField(user.getBirthdate(), UserUseCaseConstant.DATA_BIRTHDATE);
        UserValidation.validateRequiredField(user.getEmail(), UserUseCaseConstant.DATA_EMAIL);
        UserValidation.validateRequiredField(user.getPassword(), UserUseCaseConstant.DATA_PASSWORD);
        UserValidation.validateEmailValid(user.getEmail());
        UserValidation.validatePhoneLength(user.getPhone());
        UserValidation.validatePhone(user.getPhone());
        UserValidation.validateNumberDocument(user.getNumberDocument());
        UserValidation.validateAdult(user.getBirthdate(), LocalDate.now());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(newPassword);
        user.setPhone(user.getPhone().replace(UserUseCaseConstant.TELEPHONE_PREFIX, UserUseCaseConstant.TELEPHONE_PREFIX + UserUseCaseConstant.SPACE));
        user.setIdRole(idRole);
        return userPersistencePort.saveUser(user);
    }

    @Override
    public void saveUserOwner(User user) {
        saveUser(user, UserUseCaseConstant.ROLE_FOR_OWNER);
    }

    @Override
    public Long saveUserEmployee(User user) {
        return saveUser(user, UserUseCaseConstant.ROLE_FOR_EMPLOYEE);
    }

    @Override
    public void saveUserCustomer(User user) {
        saveUser(user, UserUseCaseConstant.ROLE_FOR_CUSTOMER);
    }

    @Override
    public boolean existUserWithRoleOwner(Long idUser) {
        User user = UserValidation.validateUserPresent(userPersistencePort.getUserById(idUser));
        return UserValidation.validateIdRoleOwner(user.getIdRole());
    }

    @Override
    public boolean existUserWithId(Long id) {
        UserValidation.validateUserPresent(userPersistencePort.getUserById(id));
        return true;
    }

    @Override
    public User getUserByEmail(String email) {
        return userPersistencePort.getUserByEmail(email);
    }

    @Override
    public User getUserById(Long id) {
        return UserValidation.validateUserPresent(userPersistencePort.getUserById(id));
    }
}
