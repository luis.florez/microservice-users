package com.example.microserviceusers.domain.model.constant;

public class UserUseCaseConstant {

    private UserUseCaseConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final String DATA_NAME = "Name";
    public static final String DATA_LAST_NAME = "Last name";
    public static final String DATA_DOCUMENT_NUMBER = "Document number";
    public static final String DATA_PHONE = "Phone";
    public static final String DATA_BIRTHDATE = "Birthdate";
    public static final String DATA_EMAIL = "Email";
    public static final String DATA_PASSWORD = "Password";
    public static final String TELEPHONE_PREFIX = "+57";
    public static final String SPACE = " ";
    public static final Long ROLE_FOR_OWNER = 1L;
    public static final Long ROLE_FOR_EMPLOYEE = 2L;
    public static final Long ROLE_FOR_CUSTOMER = 3L;
}
