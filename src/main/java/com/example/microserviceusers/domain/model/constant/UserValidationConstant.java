package com.example.microserviceusers.domain.model.constant;

public class UserValidationConstant {

    private UserValidationConstant() {
        throw new IllegalStateException(EXCEPTION_UTILITY_CLASS);
    }

    public static final String EXCEPTION_UTILITY_CLASS = "Utility class";
    public static final Long ROLE_FOR_OWNER = 1L;
    public static final String REGEX_FOR_VALIDATE_PHONE = "^\\+?[0-9]+(?: [0-9]+)?$";
    public static final String REGEX_FOR_VALIDATE_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String REGEX_FOR_VALIDATE_NUMBER_DOCUMENT = "^[0-9]+$";
    public static final Long MAXIMUM_LENGTH_ALLOWED_FOR_PHONE = 13L;
    public static final Long MINIMUM_LENGTH_ALLOWED_FOR_PHONE = 7L;
    public static final Long MAXIMUM_AGE_ALLOWED = 18L;
}
