package com.example.microserviceusers.domain.validation;

import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.domain.model.constant.UserValidationConstant;
import com.example.microserviceusers.infrastructure.exceptions.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.regex.Pattern;

public class UserValidation {

    private UserValidation() {
        throw new IllegalStateException(UserValidationConstant.EXCEPTION_UTILITY_CLASS);
    }

    public static User validateUserPresent(User user) {
        if (user == null) throw new UserEmptyException();
        else return user;
    }

    public static boolean validateIdRoleOwner(Long idRole) {
        return idRole.equals(UserValidationConstant.ROLE_FOR_OWNER);
    }

    public static void validateRequiredField(Object data, String typeData) {
        if (data == null) throw new FieldRequiredException(typeData);
    }

    public static void validatePhone(String phone) {
        Pattern pattern = Pattern.compile(UserValidationConstant.REGEX_FOR_VALIDATE_PHONE);
        if (!pattern.matcher(phone).find()) throw new InvalidPhoneException();
    }

    public static void validatePhoneLength(String phone) {
        if (phone.length() < UserValidationConstant.MINIMUM_LENGTH_ALLOWED_FOR_PHONE || phone.length() > UserValidationConstant.MAXIMUM_LENGTH_ALLOWED_FOR_PHONE) throw new PhoneLengthException();
    }

    public static void validateNumberDocument(String numberDocument) {
        Pattern pattern = Pattern.compile(UserValidationConstant.REGEX_FOR_VALIDATE_NUMBER_DOCUMENT);
        if (!pattern.matcher(numberDocument).find()) throw new InvalidNumberDocumentException();
    }

    public static void validateEmailValid(String email) {
        Pattern pattern = Pattern.compile(UserValidationConstant.REGEX_FOR_VALIDATE_EMAIL);
        if (!pattern.matcher(email).find()) throw new InvalidEmailException();
    }

    public static void validateAdult(LocalDate birthdate, LocalDate dateNow) {
        if (ChronoUnit.YEARS.between(birthdate, dateNow) < UserValidationConstant.MAXIMUM_AGE_ALLOWED) throw new UserNotAdultException();
    }
}
