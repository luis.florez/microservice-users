package com.example.microserviceusers.domain.api;

import com.example.microserviceusers.domain.model.User;

public interface IUserServicePort {

    void saveUserOwner(User user);

    Long saveUserEmployee(User user);

    void saveUserCustomer(User user);

    boolean existUserWithRoleOwner(Long idUser);

    boolean existUserWithId(Long id);

    User getUserByEmail(String email);

    User getUserById(Long id);
}
