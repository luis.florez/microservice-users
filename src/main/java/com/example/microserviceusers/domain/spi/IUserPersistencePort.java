package com.example.microserviceusers.domain.spi;

import com.example.microserviceusers.domain.model.User;

public interface IUserPersistencePort {

    Long saveUser(User user);

    User getUserById(Long idUser);

    User getUserByEmail(String email);
}
