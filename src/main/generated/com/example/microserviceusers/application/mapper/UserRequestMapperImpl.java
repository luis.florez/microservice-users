package com.example.microserviceusers.application.mapper;

import com.example.microserviceusers.application.dto.UserRequest;
import com.example.microserviceusers.domain.model.User;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:08:00-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class UserRequestMapperImpl implements UserRequestMapper {

    @Override
    public User toUser(UserRequest userRequest) {
        if ( userRequest == null ) {
            return null;
        }

        User user = new User();

        user.setName( userRequest.getName() );
        user.setLastName( userRequest.getLastName() );
        user.setNumberDocument( userRequest.getNumberDocument() );
        user.setPhone( userRequest.getPhone() );
        user.setBirthdate( userRequest.getBirthdate() );
        user.setEmail( userRequest.getEmail() );
        user.setPassword( userRequest.getPassword() );

        return user;
    }
}
