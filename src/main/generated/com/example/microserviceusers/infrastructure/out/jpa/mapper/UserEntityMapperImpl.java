package com.example.microserviceusers.infrastructure.out.jpa.mapper;

import com.example.microserviceusers.domain.model.User;
import com.example.microserviceusers.infrastructure.out.jpa.entity.UserEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-08-28T09:07:59-0500",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 19.0.2 (Amazon.com Inc.)"
)
@Component
public class UserEntityMapperImpl implements UserEntityMapper {

    @Override
    public UserEntity toEntity(User user) {
        if ( user == null ) {
            return null;
        }

        UserEntity userEntity = new UserEntity();

        userEntity.setId( user.getId() );
        userEntity.setName( user.getName() );
        userEntity.setLastName( user.getLastName() );
        userEntity.setNumberDocument( user.getNumberDocument() );
        userEntity.setPhone( user.getPhone() );
        userEntity.setBirthdate( user.getBirthdate() );
        userEntity.setEmail( user.getEmail() );
        userEntity.setPassword( user.getPassword() );
        userEntity.setIdRole( user.getIdRole() );

        return userEntity;
    }

    @Override
    public User toModel(UserEntity userEntity) {
        if ( userEntity == null ) {
            return null;
        }

        User user = new User();

        user.setId( userEntity.getId() );
        user.setName( userEntity.getName() );
        user.setLastName( userEntity.getLastName() );
        user.setNumberDocument( userEntity.getNumberDocument() );
        user.setPhone( userEntity.getPhone() );
        user.setBirthdate( userEntity.getBirthdate() );
        user.setEmail( userEntity.getEmail() );
        user.setPassword( userEntity.getPassword() );
        user.setIdRole( userEntity.getIdRole() );

        return user;
    }
}
